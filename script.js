let getCube = 2;
console.log(`The cube of ${getCube} is ${getCube**3}`)

let address = [ 258, "Washington Ave", "NW","California", 9011 ]
let [number, street, city, country, zip] = address
console.log(`I live at ${number} ${city} ${city}, ${country} ${zip}`);

let animal = {
	name: "Lolong",
	type: "seawater",
	breed: "crocodile",
	weight: 1075,
	length: 20,
	width:  3,
}
let {name, type, breed, weight, length, width} = animal

console.log(`${name} was a ${type} ${breed}. He weighted at ${weight} kgs with a measurement of ${length} ft ${width} in.`);


 let num = [ 1,2,3,4,5]

num.forEach(num => console.log(num))
 

 // let newNum = num.reduce((0,4))
 // console.log(newNum);

 class dog{
 	constructor(name, age, breed){
 		this.name = name;
 		this.age = age;
 		this.breed = breed;
 	}
 }
 let dogNew = new dog("Frankie", 5, "Miniature Dachshund");
 console.log(dogNew);